# Notes about the flash drives

They are all 263MB USB flash drives branded as [Vivisimo](http://en.wikipedia.org/wiki/Vivisimo) containing 3 PDFs about their software and services. They were giving them away at a local Meetup, because they wanted to get rid of them. So I asked for all of them.

There are two variants, and I explain the differences below.

Of the 130 I checked, 30 identify as Alcor Micro Corp. drives, the other 100 identify as GEMBIRD drives. The GEMBIRD drives have chipsets by AppoTech, which is the same company as used in [bunnie's microSD hacks][1].

[1]: http://www.bunniestudios.com/blog/?p=3554

## GEMBIRD PhotoFrame PF-15-1

    $ lsusb
    <snip>
    Bus 001 Device 030: ID 1908:1320 GEMBIRD PhotoFrame PF-15-1
    <snip>

![GEMBIRD PhotoFrame PF-15-1 PCB](assets/GEMBIRD_PhotoFrame_PF-15-1_PCB.jpg)

GEMBIRD [lists the PF-15-1 on their site](http://www.gembird.cn/default.aspx?op=products&op2=item&id=5162), but it claims 2MB even though this drive claims 263MB. I'm not sure what's up with this.

It [contains an AppoTech AX203 chipset][2]. bunnie's [work on hacking microSD cards][1] was with the AppoTech AX211 and AX215 chipsets.

[2]: http://picframe.spritesserver.nl/wiki/index.php/DPF_with_AppoTech_AX203

## Alcor Micro Corp. Transcend JetFlash Flash Drive

    $ lsusb
    <snip>
    Bus 001 Device 031: ID 058f:6387 Alcor Micro Corp. Transcend JetFlash Flash Drive
    <snip>

![Alcor Micro Corp. Transcend JetFlash Flash Drive](assets/Alcor_Micro_Corp_Transscend_JetFlash_Flash_Drive.jpg)

